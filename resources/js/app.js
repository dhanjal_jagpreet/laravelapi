
/*
    In this app.js, I am defining the setting up Vue router and 
    connecting it with laravel template
*/
require('./bootstrap');
import router from "./routes";
import VueRouter from "vue-router";
import Index from "./Index";
window.Vue = require('vue');

Vue.use(VueRouter); //assigning vue router set up in routes.js to Vue
const app = new Vue({
    el: '#app', // id used from laravel welcome blade
    router,
    components:{
        index : Index
    }
});
