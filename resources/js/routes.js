/*
    Here we are setting up the routes for vue
*/

import VueRouter from "vue-router";
import Youtube from "./components/Youtube";
//DEFINE ROUTES FOR THE APP:
const routes = [
    {
        path:"/",
        component: Youtube,
        name:'home'
    }
];
const router = new VueRouter({
    routes,
    mode: "history"
});

export default router;