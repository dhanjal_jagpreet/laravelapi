<?php
/*
    This class is defined to have all the youtube api functionality at one place. 
    We can add more function here if we need in future

*/
namespace App\Services\Google;
use Google_Client;
use Google_Service_YouTube;
use Illuminate\Http\Request;

class YoutubeApi {
    protected $service;

    public function __construct(Google_Client $client) //getting google client object from the custom service provider
    {
        $service = new Google_Service_YouTube($client); //creating youtube service object using Google Client
        $this->service = $service;
    }
    public function getSearchResults($searchString='', $order='date', $maxResults='10')
    {
        //SETTING UP PARAMS FOR SEARCH:
        $queryParams = [
                'maxResults' =>  $maxResults,
                'order' =>  $order,
                'q' => $searchString
            ];
            
        $response = $this->service->search->listSearch('snippet', $queryParams); //calling listSearch function to get search results
        return $response;
    }
}
