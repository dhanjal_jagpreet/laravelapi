<?php
/*
    This is YoutubeSearchResuts controller
    I am conneting with to YoutubeApi class which stores Api functionality to get the final results
*/
//IMPORTING REQUIRED CLASSES AND NAMESPACES:
namespace App\Http\Controllers;
use Google_Service_YouTube;
use Illuminate\Http\Request;
use App\Services\Google\YoutubeApi;

class YoutubeSearchResults extends Controller
{
    protected $service;

    public function __construct(YoutubeApi $service) //taking YoutubeApi service object as param, we can use it here because we had bind it in custom service provider
    {
       $this->service = $service;
    }
    function show($query){
        $results = $this->service->getSearchResults($query); //get the search results
        $items = $results['modelData']['items'];
        return $items; 
    }
}
