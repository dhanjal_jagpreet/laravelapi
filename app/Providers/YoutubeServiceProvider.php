<?php
/*
    This is a custom service provider created for youtube api
    This file set ups Google Client and bind it with Youtube Api Class. 
    Which keeps the google client setup and youtube service separate and clean.
*/

namespace App\Providers;
use Google_Client;
use App\Services\Google\YoutubeApi;
use Google_Service_YouTube;
use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\YoutubeSearchResults;


class YoutubeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
   
    public function register()
    {
        $this->app->singleton(YoutubeApi::class, function ($app) { //binds this with youtubeApi
            $client = new Google_Client(); //creating google client
            $client->setApplicationName(config('services.youtubeApi.name'));
            $client->setDeveloperKey(config('services.youtubeApi.key'));
            return new YoutubeApi($client); //returing google client object to Youtube Api class
         });

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
    }
}
